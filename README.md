# home_credit_ensembles_overview

## Краткое описание
Изучение некоторых закономерностей алгоритмов стэкинга и бустинга

## Язык реализации
Python 3.7.8
## Используемые сторонние модули
* numpy==1.21.2
* pandas==1.3.3
* scikit-learn==1.0
* plotly==5.3.1
* lightgbm

## Начало работы
Установить необходимые модули

!sudo pip3 install -r requirenments.txt

В директории проекта sber_housing_market необходимо создать следующую структуру папок
* home_credit_ensembles 
 + notebooks
 + data
    - raw

После этого поместить файл homework.ipynb в папку notebooks. Также необходимо скачать данные с https://www.kaggle.com/c/home-credit-default-risk/data и расположить в папке data/raw

После этого можно открывать файл homework.ipynb и работать с ним в обычном режиме
